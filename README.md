# license-header

**Made with <3 by [stdmatt](http://stdmatt.com).**

## Description:

```license-header``` is a tool that gathers information about a file and generate a 
header comment for it. It tries to get information directly from ```git``` if available 
but fallback to filesystem metadata if it's not possible.

I created it to enable me to have consistency among my files without needing to do 
_"monkey job"_ every time... 

<br>

As usual, you are **very welcomed** to **share** and **hack** it.

## Examples:

```shell script
## This make license-header grab the information and insert the header on the file...
license-header ./super_nice.c --inline

## Now imagine that the file is renamed...
mv ./super_nice.c ./hyper_nice.c

## I can just run the license-header again and don't worry about the things...
license-header ./hyper_nice.c --inline
```

<p align="center">
    <img src="./res/license_header.gif"/>
</p>



## Usage:
```shell script
Usage:
  license-header [--help] [--version]
  license-header [--just-header]  <filename> [... filenames]
  license-header [--inline]       <filename> [... filenames]

Options:
  *-h --help     : Show this screen.
  *-v --version  : Show program version and copyright.

  --just-header : Print to stdout just the generated header.
  --inline      : Modify the input file prepeding the generated header.

Notes:
  <filename> must always be specified.

  If no flags are given, the default operation is to print the generated
  header and the previous content of the file into the stdout.

  --just-header takes precedence over --inline.

  Options marked with * are exclusive, i.e. the license-header will run that
  and exit after the operation.
```


## Install:

```shell script
## 1- Clone...
$ git clone https://gitlab.com/stdmatt-personal/license_header.git
## 2 - Go to directory...
$ cd ./license_header
## 3 - Install...
$ ./install.sh ## sudo password will be prompted to you
## 4 - Have fun ;D
```

## License:

This software is released under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).


## Others:

There's more FLOSS things at [stdmatt.com](https://stdmatt.com) :)
